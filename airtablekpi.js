function formatDate(inputDate) {
	// Format the date components into "dd-mm-yyyy" format
	const dd = String(inputDate.getDate()).padStart(2, "0");
	const mm = String(inputDate.getMonth() + 1).padStart(2, "0"); // Month is zero-based
	const yyyy = inputDate.getFullYear();

	return `${dd}-${mm}-${yyyy}`;
}

// Ask the user to import a CSV file containing a header row
let csvFileResult = await input.fileAsync("Upload a CSV file", {
	allowedFileTypes: [".csv"],
	hasHeaderRow: true,
});

// The file importer will automatically parse contents for many file types, including CSV files
let csvRows = csvFileResult.parsedContents;

// Edit this to the name of a table in your base
let table = base.getTable("KPI");

let shouldContinue = await input.buttonsAsync(
	`Import ${csvRows.length} records from ${csvFileResult.file.name} into ${table.name}?`,
	[{ label: "Yes", variant: "primary" }, "No"]
);

if (shouldContinue === "Yes") {
	// Get our table records
	let table = base.getTable("KPI");
	let query = await table.selectRecordsAsync({ fields: ["key"] });
	let kpiTableData = query.records;

	// Get all existing campaign keys
	let existingCampaigns = kpiTableData.map((record) =>
		record.getCellValue("key")
	);

	let newRecords = [];
	let updateRecords = [];
	// Check all csv fields for new and existing records
	csvRows.forEach((record) => {
		// Generate key according to formula
		const recordKey =
			formatDate(record["Date Time"]) + "_" + record["Campaign ID"];
		output.text(recordKey);
		if (existingCampaigns.indexOf(recordKey) >= 0) {
			updateRecords.push({ id: recordKey, fields: { ...record } });
		} else {
			newRecords.push({ fields: { ...record } });
		}
	});

	// A maximum of 50 record edits are allowed at one time, so do it in batches
	while (updateRecords.length > 0) {
		await table.updateRecordsAsync(updateRecords.slice(0, 50));
		updateRecords = updateRecords.slice(50);
	}

	// A maximum of 50 record creations are allowed at one time, so do it in batches
	while (newRecords.length > 0) {
		await table.createRecordsAsync(newRecords.slice(0, 50));
		newRecords = newRecords.slice(50);
	}
}
